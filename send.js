const { default: axios } = require('axios');
const fs = require('fs');
const axiosRetry = require('axios-retry');

axiosRetry(axios, { retries: 3 });

let rawdata = fs.readFileSync('vendas_api.json');
let toApi = JSON.parse(rawdata);

const delay = (ms = 5000) => new Promise((r) => setTimeout(r, ms));
const sendSales = async function (items) {
    let results = [];
    for (let index = 0; index < items.length; index++) {
        console.log("Enviando " + (index + 1) + " de " + items.length + " - [ID: " + items[index]['unique_id'] + ']');

        await delay();
        const res = axios.post('http://connectplug.com.br/api/v2/app/sale', items[index], {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': '',
                'Device-ID': '00000000-0000-0000-0000-000000000006'
            },
        });

        res.then(() => {
            let raw = fs.readFileSync('vendas_api.json');
            let js = JSON.parse(raw);

            let i = js.findIndex((s) => items[index]['unique_id'] == s['unique_id']);

            if (i >= 0) {
                js.splice(i, 1);
            }

            fs.writeFileSync('vendas_api.json', JSON.stringify(js, null, 2));

        });

        results.push(res.data);
    }
    return results;
};

async function main(sales) {
    const results = await sendSales(sales);
}


main(toApi);
