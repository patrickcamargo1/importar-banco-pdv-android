'use strict';

const fs = require('fs');

let rawdata = fs.readFileSync('cp_pdv.json');
let data = JSON.parse(rawdata);

let toApi = data.Venda.map((sale) => {
    let mapped = {};

    //Cliente
    let customer = {};

    //Id
    customer['id'] = sale.cliente.idClienteWeb;

    //Razão social
    if (sale.pessoaJuridica && sale.cliente.nome != null) {
        customer['name'] = sale.cliente.nomeFantasia;
    } else if (!sale.pessoaJuridica && sale.cliente.nome != null) {
        customer['name'] = sale.cliente.nome;
    }

    //Documento Cliente
    customer['cpf'] = sale.cpfCliente;

    mapped['customer'] = customer;

    //Status Entrega
    if (sale.statusEntrega != null) {
        mapped['status']
    }

    //Id do Usuário
    mapped['user_id'] = 1;


    //Ponto de Venda
    mapped['pos_status_id'] = sale.idSessaoPontoVenda;

    //Vendedor
    if (sale.vendedor != null) {
        mapped['seller_id'] = sale.vendedor.idVendedor;
    }

    //Finalizar após entrega
    mapped['delivery_end_after'] = sale.finalizarAposEntrega;

    //Dados delivery
    if (sale.indexOperacao != -1) {
        mapped['flg_delivery'] = true;
        if (sale.indexOperacao == -2) {
            mapped['delivery_type'] = 'WITHDRAWAL';
        } else if (sale.indexOperacao > -1) {
            mapped['delivery_type'] = 'DELIVERY';
        } else {
            mapped['delivery_type'] = 'NA';
        }

        if (sale.taxaEntrega != null) {
            sale['delivery_tax'] = parseFloat(sale.taxaEntrega);
        }
    }

    mapped['take_away'] = sale.paraLevar;

    //Horarios
    mapped['sale_start'] = formatDate(sale.dataInicio);
    mapped['sale_end'] = formatDate(sale.dataConclusao);

    //Id venda
    mapped['unique_id'] = sale.idVenda;

    let totalExchange = 0;

    mapped['payments'] = sale.pagamentoVendas.map((p) => {

        totalExchange += round(parseFloat(p.valorTotal));

        return p;
    });

    let totalWithDiscount = 0;
    let totalDiscount = 0;

    mapped['products'] = sale.produtosDaVenda.map(function (p) {

        let totalUnitary = round(parseFloat(p.precoVendaUnitario) * parseFloat(p.qntProduto));

        let discount = 0;

        if (p.isDescontoPorcentagem && p.desconto != null) {
            discount = totalUnitary * parseFloat(p.desconto);
        } else if (!p.isDescontoPorcentagem && p.desconto != null) {
            discount = round(parseFloat(p.desconto));
        }

        // total += round(parseFloat(p.precoVendaUnitario) * parseFloat(p.qntProduto));

        totalWithDiscount += (totalUnitary - discount);
        totalDiscount += discount;

        return {
            'id': p.produto.idProduto,
            'name': p.produto.nome,
            'observation': p.produto.observacoes,
            'amount': p.qntProduto,
            'flg_consumption_coupon': p.produto.impressaoFicha,
            'unitary_value': round(parseFloat(p.precoVendaUnitario)),
            'total_value': totalUnitary,
            'offline_sale_product_id': p.idProdutoVenda,
            'additionals': [],
            'attributes': [],
            'removedComponents': []
        };
    });


    //Todo
    let saleDiscount = 0;

    //Desconto
    if (sale.desconto > 0) {
        if (sale.isDescontoPorcentagem && sale.desconto != null) {
            saleDiscount = round(totalWithDiscount * parseFloat(sale.desconto))
        }
        else if (!sale.isDescontoPorcentagem && sale.desconto != null) {
            saleDiscount = round(parseFloat(sale.desconto))
        }
    }

    totalWithDiscount -= saleDiscount;

    mapped['discount'] = saleDiscount;

    mapped['combos'] = [];

    //Taxa
    mapped['tax_value'] = 0;

    //Troco
    mapped['exchange'] = 0;

    //Valor Total
    mapped['total_value'] = round(totalWithDiscount);


    mapped['payments'] = sale.pagamentoVendas.map((p) => {

        return {
            'payment_method_id': p.idMetodoPagamento,
            'method': getMetodoPagamento(p.idMetodoPagamento),
            'total': round(parseFloat(p.valorTotal)),
            'nfe_code': '01',
            'installments': [
                {
                    'due_date': formatDate(sale.dataInicio),
                    'value': mapped['total_value'],
                }
            ],
            //Parametros tef
        };
    });

    //Fim todo
    return mapped;
});

fs.writeFileSync('vendas_api.json', JSON.stringify(toApi, null, 2));


function formatDate(unixtime) {
    let date_ob = new Date(unixtime);

    // adjust 0 before single digit date
    let date = ("0" + date_ob.getDate()).slice(-2);

    // current month
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

    // current year
    let year = date_ob.getFullYear() < 10 ? '0' + date_ob.getFullYear() : date_ob.getFullYear();

    // current hours
    let hours = date_ob.getHours() < 10 ? '0' + date_ob.getHours() : date_ob.getHours();

    // current minutes
    let minutes = date_ob.getMinutes() < 10 ? '0' + date_ob.getMinutes() : date_ob.getMinutes();

    // current seconds
    let seconds = date_ob.getSeconds() < 10 ? '0' + date_ob.getSeconds() : date_ob.getSeconds();

    return year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
}

function getMetodoPagamento(idMetodoPagamento) {
    if (idMetodoPagamento == 1) {
        return "MONEY";
    } else if (idMetodoPagamento == 2) {
        return "CHECK";
    } else if (idMetodoPagamento == 3) {
        return "CREDIT";
    } else if (idMetodoPagamento == 4) {
        return "DEBIT";
    } else if (idMetodoPagamento == 5) {
        return "BANK_SLIP";
    } else if (idMetodoPagamento == 6) {
        return "DEPOSIT";
    } else if (idMetodoPagamento == 7) {
        return "PAGSEGURO";
    } else if (idMetodoPagamento == 8) {
        return "ACCOUNT_DEBIT";
    } else if (idMetodoPagamento == 10) {
        return "VOUCHER";
    } else if (idMetodoPagamento == 12) {
        return "STORE_CREDIT";
    } else if (idMetodoPagamento == 13) {
        return "BILLING";
    } else {
        return "OTHERS";
    }
}

function round(num) {
    return parseFloat(Math.round((num + Number.EPSILON) * 100) / 100);
}